﻿using System;
using System.Windows.Forms;
using team_work.Classes;

namespace team_work.Interface
{
    public class ContractPanel : Panel
    {
        private Label lblName;
        private Label lblCapacity;
        private Label lblPrice;
        private Label lblFDate;
        private Label lblStatus;
        private Label lblRegular;
        private Label lblDistance;
        private Button btnDo;
        private Button btnCancel;
        private Flight flight;
        private bool Arcived;

        #region Настройка панели
        private void Setup()
        {
            lblName = new Label();
            lblFDate = new Label();
            lblPrice = new Label();
            lblCapacity = new Label();
            lblStatus = new Label();
            lblRegular = new Label();
            lblDistance = new Label();
            btnDo = new Button();
            btnCancel = new Button();

            Location = new System.Drawing.Point(3, 3);
            Size = new System.Drawing.Size(270, 150);
            TabIndex = 0;
            BackColor = System.Drawing.Color.PaleGreen;
            // 
            // lblName
            // 
            lblName.AutoSize = true;
            lblName.BackColor = System.Drawing.Color.Transparent;
            lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
            lblName.Location = new System.Drawing.Point(30, 19);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(66, 24);
            lblName.TabIndex = 0;
            // 
            // lblDistance
            // 
            lblDistance.AutoSize = true;
            lblDistance.BackColor = System.Drawing.Color.Transparent;
            lblDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblDistance.Location = new System.Drawing.Point(160, 19);
            lblDistance.Name = "lblDistance";
            lblDistance.Size = new System.Drawing.Size(66, 24);
            lblDistance.TabIndex = 0;
            // 
            // lblFDate
            // 
            lblFDate.AutoSize = true;
            lblFDate.BackColor = System.Drawing.Color.Transparent;
            lblFDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblFDate.Location = new System.Drawing.Point(30, 50);
            lblFDate.Name = "lblFDate";
            lblFDate.Size = new System.Drawing.Size(67, 24);
            lblFDate.TabIndex = 1;
            // 
            // lblPrice
            // 
            lblPrice.AutoSize = true;
            lblPrice.BackColor = System.Drawing.Color.Transparent;
            lblPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblPrice.Location = new System.Drawing.Point(150, 50);
            lblPrice.Name = "lblPrice";
            lblPrice.Size = new System.Drawing.Size(53, 20);
            lblPrice.TabIndex = 2;
            // 
            // lblCapacity
            // 
            lblCapacity.AutoSize = true;
            lblCapacity.BackColor = System.Drawing.Color.Transparent;
            lblCapacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblCapacity.Location = new System.Drawing.Point(30, 80);
            lblCapacity.Name = "lblCapacity";
            lblCapacity.Size = new System.Drawing.Size(53, 20);
            lblCapacity.TabIndex = 3;
            // 
            // lblRegular
            // 
            lblRegular.AutoSize = true;
            lblRegular.BackColor = System.Drawing.Color.Transparent;
            lblRegular.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblRegular.Location = new System.Drawing.Point(150, 70);
            lblRegular.Name = "lblRegular";
            lblRegular.Size = new System.Drawing.Size(53, 20);
            lblRegular.TabIndex = 3;
            // 
            // btnDo
            // 
            btnDo.Location = new System.Drawing.Point(30, 110);
            btnDo.Name = "btnDo";
            btnDo.Size = new System.Drawing.Size(80, 25);
            btnDo.TabIndex = 4;
            btnDo.Text = "Выполнить";
            btnDo.UseVisualStyleBackColor = true;
            btnDo.Click += btnTake_Click;
            // 
            // btnCancel
            // 
            btnCancel.Location = new System.Drawing.Point(150, 110);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(80, 25);
            btnCancel.TabIndex = 4;
            btnCancel.Text = "Отмена";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // lblStatus
            // 
            lblStatus.AutoSize = true;
            lblStatus.BackColor = System.Drawing.Color.Transparent;
            lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblStatus.Location = new System.Drawing.Point(30, 110);
            lblStatus.Name = "lblCapacity";
            lblStatus.Size = new System.Drawing.Size(53, 20);
            lblStatus.TabIndex = 3;
            lblStatus.Visible = false;
        }
        #endregion


        private void btnCancel_Click(object sender, EventArgs e)
        {
            (Application.OpenForms[0] as MainForm).game.CancelContract(flight);
            (Application.OpenForms[0] as MainForm).CancelContract(this, flight);
        }

        private void btnTake_Click(object sender, EventArgs e)
        {            
            var game = (Application.OpenForms[0] as MainForm).game;
            game.Wait();
            ContractForm cf = new ContractForm();
            cf.Init(game.GetAirplanesForContract(flight), game.Date.Hour);
            if (cf.ShowDialog() == DialogResult.OK)
            {
                flight.StartContract(cf.StartHour, cf.Chosen, game.Date);
                UpdateInfo(game.Date);
            }
            game.Continue();
        }
        public ContractPanel(Flight flight, bool Arcive)
        {
            this.flight = flight;
            Setup();
            lblName.Text = flight.StartCity + " - " + flight.EndCity;
            lblFDate.Text = flight.StartDate.ToLongDateString();
            lblPrice.Text = "Цена: $" + flight.Price.ToString();
            lblRegular.Visible = false;
            if (flight.Type is AirlanerType)
            {
                lblCapacity.Text = "Пассажиры " + flight.Type.CapacityRequired;
                var f = (flight.Type as AirlanerType);
                if (f.Regular)
                {
                    lblRegular.Text = "Регуляный\nраз в " + f.Regularity + DaysLabel(f.Regularity);
                    lblRegular.Visible = true;
                }
            }
            else
            {
                lblCapacity.Text = "Груз " + flight.Type.CapacityRequired;
            }
            lblDistance.Text = $"({flight.Distance} км)";
            this.Arcived = Arcive;
            Controls.Add(btnDo);
            Controls.Add(btnCancel);
            Controls.Add(lblCapacity);
            Controls.Add(lblPrice);
            Controls.Add(lblFDate);
            Controls.Add(lblName);
            Controls.Add(lblStatus);
            Controls.Add(lblRegular);
            Controls.Add(lblDistance);
            UpdateInfo((Application.OpenForms[0] as MainForm).game.Date);
        }
        private string ProgressLabel(int h)
        {
            if (h == 1) return " час полёта\n остался";
            if (h < 5) return " часа полёта\n осталось";
            return " часов полёта\n осталось";
        }
        private string DaysLabel(int d)
        {
            if (d == 1) return " день";
            if (d < 5) return " дня";
            return " дней";
        }
        private void Archivate()
        {
            if (!Arcived)
            {
                (Application.OpenForms[0] as MainForm).ArchiveContract(this);
                (Application.OpenForms[0] as MainForm).game.ArciveContract(flight);
            }
            Arcived = true;
        }
        public void UpdateInfo(DateTime now)
        {
            int x = (int)(flight.EndDate - now).TotalHours;
            switch (flight.Status)
            {
                case FlightStatus.Wait:
                    lblStatus.Visible = true;
                    lblStatus.Text = "ждем " + (int)(flight.StartDate - now).TotalHours;
                    btnDo.Visible = false;
                    break;
                case FlightStatus.Progress:
                    lblStatus.Text = x + ProgressLabel(x);
                    lblStatus.Visible = true;
                    btnDo.Visible = false;
                    btnCancel.Visible = false;
                    break;
                case FlightStatus.Complete:
                {
                    if ((flight.Type is AirlanerType) && (flight.Type as AirlanerType).Regular)
                    {
                        btnDo.Visible = true;
                        btnCancel.Visible = true;
                        lblStatus.Visible = false;
                    }
                    else
                    {
                        lblStatus.Visible = true;
                        lblStatus.Text = "выполнен";
                        btnDo.Visible = false;
                        btnCancel.Visible = false;
                        Archivate();
                    }
                   
                    break;
                }
                    
                case FlightStatus.None:
                {
                    if ((flight.Type is AirlanerType) && (flight.Type as AirlanerType).Regular)
                    {
                        var game = (Application.OpenForms[0] as MainForm).game;
                        var f = flight.Type as AirlanerType;
                        int d = (f.NextDate.Date - game.Date.Date).Days;
                        if (d > 0)
                        {
                            lblStatus.Text = "ждём " + d + DaysLabel(d);
                            lblStatus.Visible = true;
                            btnDo.Visible = false;
                            btnCancel.Visible = true;
                        }
                        else
                        {
                            lblStatus.Visible = false;
                            btnDo.Visible = true;
                            btnCancel.Visible = true;
                        }
                    }
                    break;
                }
                case FlightStatus.Expired:
                    lblStatus.Text = "провален";
                    lblStatus.Visible = true;
                    btnDo.Visible = false;
                    btnCancel.Visible = false;
                    Archivate();
                    break;
                case FlightStatus.WaitReturn:
                    lblStatus.Text = "ждем возврат (час)";
                    break;
                case FlightStatus.Return:
                    lblStatus.Text = "возврат: " + x + ProgressLabel(x);
                    break;
                default:
                    break;
            }

        }
    }
}
